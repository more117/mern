var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    Comment = require('./model/comments');

var app = express();

var router = express.Router();
var port = process.env.API_PORT || 3001;

// Database connect
mongoose.connect('mongodb://127.0.0.1:27017/carlosdb');

// configure API to use bodyParser and look for JSON data in request body

//now we should configure the API to use bodyParser and look for 
//JSON data in the request body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//To prevent errors from Cross Origin Resource Sharing, we will set 
//our headers to allow CORS with middleware like so:
app.use((req, res, next) => {
 res.setHeader('Access-Control-Allow-Origin', '*');
 res.setHeader('Access-Control-Allow-Credentials', 'true');
 res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
 res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
//and remove cacheing so we get the most recent comments
 res.setHeader('Cache-Control', 'no-cache');
 next();
});
//now we can set the route path & initialize the API
router.get('/', (req, res) => {
 res.json({ message: 'API Initialized!'});
});

//adding the comments route to our api router
router.route('/comments')
.get((req,res)=>{
    Comment.find((err, comments)=>{
        if(err) res.send(err);
        res.json(comments);
    })
})
.post((req,res)=>{
    var comment = new Comment();
    comment.author = req.body.author;
    comment.text = req.body.text;

    comment.save((err)=>{
        if(err) res.send(err)
        res.json({
            message: 'Comments succesfully added!'
        });
    });
})

//Use our router configuration when we call /api
app.use('/api', router);

// starts the server and listen for request
app.listen(port, () => {
    console.log('api running on port: ', port);
});