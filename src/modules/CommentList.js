import React from 'react';
import Comment from './Comment';
import style from './../style';

const CommentList = (props) => {
    console.log('CommentList props: ',props.data)
    let commentNodes = props.data.map(comment =>{
        return(
            <Comment author={comment.author} key={comment['_id']}>
                {comment.text}
            </Comment>
        );
    });
    return(
        <div style={style.commentList}>
            {commentNodes}
        </div>
    );
}

export default CommentList;